package src

import (
	"fmt"
	"os"

	tea "github.com/charmbracelet/bubbletea"
)

type model struct {
}

func (m *model) Init() tea.Cmd {
	return nil
}

func (m *model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c":
			return m, tea.Quit
		}
	}

	return m, nil
}

func (m *model) View() string {

	return "Hello from Cobra + Tea!"
}

func Execute() {
	teaErr := tea.NewProgram(&model{}, tea.WithAltScreen()).Start()
	if teaErr != nil {
		fmt.Fprintln(os.Stderr, teaErr)
		os.Exit(1)
	}
}
