package counter

import (
	"fmt"
	"os"

	tea "github.com/charmbracelet/bubbletea"
)

type model struct {
	count int
}

func (m *model) Init() tea.Cmd {
	return nil
}

func (m *model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {

	switch msg := msg.(type) {
	case tea.KeyMsg:

		// if input is a string
		switch msg.String() {

		// required to exit the bubbletea instance
		case "ctrl+c":
			return m, tea.Quit

		case "up":
			m.count++

		case "down":
			m.count--
		}

	}

	return m, nil
}

func (m *model) View() string {

	return fmt.Sprintf(`
count: %d
	
↑ increase		↓ decrease

Press Ctrl+C to exit
	`, m.count)
}

func Execute() {
	teaErr := tea.NewProgram(&model{}, tea.WithAltScreen()).Start()
	if teaErr != nil {
		fmt.Fprintln(os.Stderr, teaErr)
		os.Exit(1)
	}
}
