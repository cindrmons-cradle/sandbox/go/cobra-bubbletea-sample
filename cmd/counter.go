/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"cobra-bubbletea-sample/src/counter"

	"github.com/spf13/cobra"
)

// counterCmd represents the counter command
var counterCmd = &cobra.Command{
	Use:   "counter",
	Short: "Counts upward and downward.",
	Long:  `A sample command made in bubble tea that shows incrementing and decrementing a value.`,
	Run: func(cmd *cobra.Command, args []string) {
		counter.Execute()
	},
}

func init() {
	rootCmd.AddCommand(counterCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// counterCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// counterCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
