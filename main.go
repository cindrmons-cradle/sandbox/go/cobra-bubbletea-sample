/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import "cobra-bubbletea-sample/cmd"

func main() {
	cmd.Execute()
}
